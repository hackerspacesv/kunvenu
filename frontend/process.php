<?php
/*
 * Kunvenu: An offline, web-based, multi-user/multi-board graphical
 *          development environment for Arduino.
 *
 * Kunvenu means "congregate" in Esperanto.
 *
 * Copyright 2018 Mario Gomez @ Hackerspace San Salvador
 * This file is part of Kunvenu.
 *
 * License: Kunvenu is free software: you can distribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Kunvenu is distribute in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without event the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Kunvenu. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/*
 * This file is where all the magic happens.
 * This endpoint is in charge of compiling and loading
 * the sketchs from the different users. It mantains
 * a separate build environment for each user in a way
 * that multiple concurrent users could use the same
 * machine to upload their graphical sketchs.
 */
header("Content-Type: application/json");

// Set arduino-cli path:
$arduino_cli = "/usr/bin/arduino-cli";

// Set base work directory:
$work_base = "/var/www/html/work";

// Set the SQLite3 device database:
$device_db = "device_db.db";

// Array with response data
$response = array('result'=>array());

// Check if input data matches (hexadecimal 4-digit id)
if(!(isset($_GET['id']) && (preg_match('/^[a-f0-9]{4}$/i', $_GET['id'])==1))) {
  // Set error message and exit if board ID  isn't valid
  $response['result'] = "Error: Invalid board ID.";
  echo json_encode($response);
  exit(0);
}

$board_id = strtoupper($_GET['id']);

// Now with a valid board ID we build all the remaining paths
$work_dir = "$work_base/work$board_id";
$sketch_dir = "$work_dir/Sketch";
$build_dir = "$work_dir/build";
$cache_dir = "$work_dir/core_build";

// Create all the required directories (in case they not exist)
mkdir($work_dir, 0755, true);
mkdir($sketch_dir, 0755, true);
mkdir($build_dir, 0755, true);
mkdir($cache_dir, 0755, true);

// Check if all directories exists
if(!(
  file_exists($work_dir) && is_dir($work_dir) &&
  file_exists($sketch_dir) && is_dir($sketch_dir) &&
  file_exists($build_dir) && is_dir($build_dir) &&
  file_exists($cache_dir) && is_dir($cache_dir)
  )) {
  // Set error message and exist if work directories cannot be created
  $response['result'] = "Error: Cannot create work directories";
  echo json_encode($response);
  exit(0);
}

// TODO: Check DB status if a compilation is already running
// abort if that's the case

if(!isset($_POST['sketch'])) {
  // End with error if no sketch content is set.
  $response['result'] = "Error:  Sketch content not set";
  echo json_encode($response);
  exit(0);
}

// Now copy the request contents to the Sketch file
$sketch_file = fopen("$sketch_dir/Sketch.ino", 'w');
fwrite($sketch_file, $_POST['sketch']);
fclose($sketch_file);

// Get the usb device assigned to the device
// TODO: Check if database file can be opened
class DeviceDB extends SQLite3
{
  function __construct()
  {
    $this->open('device_db.db');
  }
}

$dev_db = new DeviceDB();

echo $dev_db->lastErrorMsg();

$db_query = "SELECT device FROM devices WHERE serial LIKE '%$board_id'";

$result = $dev_db->querySingle($db_query, true);

if($result) { // We got a match!
  $port = $result['device'];
  $cmd_output = '';

  $cmd_output .= shell_exec("$arduino_cli compile -v --build-cache-path $cache_dir --build-path $build_dir --fqbn arduino:avr:uno $sketch_dir");
  $cmd_output .= shell_exec("$arduino_cli upload -p $port --fqbn arduino:avr:uno $sketch_dir");

  $dev_db->close();
  $response['result'] = 'Commands completed';
  $response['cmd_out'] = $cmd_output;
  echo json_encode($response);
  exit(0);
} else {
  // If we don't find a match it means that the device was not detected
  // by the system
  $dev_db->close();
  $response['result'] = 'Error: Device not detected';
  echo json_encode($response);
  exit(0);
}
