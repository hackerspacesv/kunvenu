# Kunvenu
Kunvenu is an offline, web-based, simultaneous multi-user, multi-board block programming interface for Arduino.

This project is meant to be used in classrooms where results impractical or just too costly to get a whole computer lab to teach basic electronics with Arduino but were is easy for the students to access a basic smartphone.

Kunvenu acts as an Access Point for the classroom. When the students connect to the AP the teacher can instruct them to access the default website available on the network and create simple sketchs using a Blocky-based interface. Kunvenu supports multiple concurrent users, this way only one computer with an USB hub can simultaneously program multiple Arduino boards at the same time.

Kunvenu was designed to not require Internet access, neither it requires to install any Application on the student phone. This means that can be used even with very low-end Android-based smartphones.

## Hardware Required

Any computer with a recent linux distribution should be supported. This implementation was tested on a small Raspberry Pi with Raspbian Stretch. Current configuration files could be used as-is on debian-based distributions. Small changes would be need for other distributions.

Currently only original Arduino boards are supported (Please see the explanation for this at the end).

**Optional:** By default the configuration scripts are going to set-up the default wireless interface as an Access Point, however better range and signal could be achieved with a external wireless adapter. If you choose this way please check that the adapter is supported by hostapd.

## Software Required
Please install the following software with the package manager included with your distribution:
 - hostapd
 - dnsmasq
 - nginx
 - PHP7
 - SQLite3

## Installation
We are currently working on a detailed step-by-step install document and an automatic installer. Meanwhile you can take a look to the configuration finles under the directory [config_files/etc](./config_files/etc).

## How to use it
Before start to use kunvenu you'll need to identify the serial number on your Arduino Boards first. There are two ways you can do this:

### Using the Arduino IDE
 1. Connect your board and open the Arduino IDE
 2. Go to the menu **"Tools/Port"** and select the port where your Arduino is connected.
 3. Click on **"Tools/Get board information"** and take note of the last 4 letters of the serial number

### Using the console on GNU/Linux
1. Open a console
2. Run the following command (replace ttyACM0 with the port where arduino is connected)
    > udevadm info /dev/ttyACM0

3. Take note of the last 4 letters under **"ID_SERIAL_SHORT"**

### Programming with kunvenu

 1. Connect to the **"kunvenu"** network.
 2. Open the following address in your browser:
    > http://lab/

If for some reason it doesn't works try to open the following address:
> http://192.168.254.1/

3. Once you are there you can use the blocks to create your program.
4. Put the last 4 letters of the Arduino Serial number and click "Save".

You'll receive a message if the sketch was successfully uploaded or an error message if there is any problem.

## Why only support original Arduinos?
Original Arduinos include a unique Serial Number that can be used to identify each individual Arduino connected to the host computer. Arduino-Compatible boards usually doesn't include any unique serial number under their USB-Identification information. This is a problem for kunvenu because we don't have any "easy way" to disambiguate the port used to connect the board and to guarantee that the sketch ends in the right board.

We are currently working to remove this limitation for future releases.

## License 
Kunvenu is an offline, web-based, simultaneous multi-user, multi-board block programming interface for Arduino.

Kunvenu means "congregate" in Esperanto.

Copyright 2018 Mario Gomez @ Hackerspace San Salvador

Kunvenu is free software: you can distribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Kunvenu is distribute in the hope that it will be useful, but WITHOUT ANY WARRANTY; without event the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Kunvenu. If not, see <https://www.gnu.org/licenses/>.
