#!/bin/bash
DBFILE=/var/www/html/device_db.db

# Check if both parameters are set
if [ -z "$1" ] || [ -z "$2" ]
then
echo "ERROR: You must define both an action (ADD|REMOVE) and a device name."
echo "Example: update_device_db.sh ADD ttyACM0"
exit 1
fi

# Check if a valid command was specified
if [[ ( "$1" -ne "ADD" ) || ( "$1" -ne "REMOVE" ) ]]
then
echo "ERROR: Invalid action specified."
exit 1
fi

DEV_STATUS=0

if [ "$1" = "REMOVE" ]
then
DEV_STATUS=9
fi

if [ "$1" = "ADD" ]
then

# Extract the serial number from the device info
SERIAL_NUMBER=`udevadm info --name=$2 | grep 'ID_SERIAL_SHORT' | grep -Po '[0-9A-F][0-9A-F]+'`
# If the device contains a valid serial number then add it to the DB
if [ ! -z "$SERIAL_NUMBER" ]
then
sqlite3 $DBFILE "INSERT OR REPLACE INTO devices(serial,device,status) VALUES('$SERIAL_NUMBER','$2',$DEV_STATUS);"
fi

# End check for add
fi

# Update status if we need to remove a device
if [ "$1" = "REMOVE" ]
then
sqlite3 $DBFILE "UPDATE devices SET status=9 WHERE device='$2' AND STATUS!=9;"
fi


exit 0
